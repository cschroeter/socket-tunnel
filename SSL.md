certbot -d my.edomo.de -d "\*.my.edomo.de" --server https://acme-v02.api.letsencrypt.org/directory --manual --preferred-challenges dns certonly

kubectl create secret tls my-edomo-tls --key privkey.pem --cert cert.pem --namespace socket-tunnel
